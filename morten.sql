DROP DATABASE if EXISTS worldTest;
CREATE DATABASE worldTest;
USE worldTest;
CREATE TABLE city (
    id INT not null,
    city_name VARCHAR(50) not null,
    city_population INT not null,
    district VARCHAR(50) not null,
    country_isocode INT not null,
    primary key(id),
    foreign key(country_code) REFERENCES country(code)
);
CREATE TABLE country (
    isocode CHAR(3) NOT NULL,
    code2 INT NOT NULL,
    country_name VARCHAR(60) NOT NULL,
    local_name VARCHAR(60) NOT NULL,
    contry_population INT NOT NULL,
    life_expectancy DECIMAL(2, 1) NOT NULL,
    continent VARCHAR(50) NOT NULL,
    region VARCHAR(50) NOT NULL,
    head_of_state VARCHAR(50) NOT NULL,
    government_form VARCHAR(50) NOT NULL,
    gdp DECIMAL(19, 4) NOT NULL,
    gdp_old DECIMAL(19, 4) NOT NULL,
    surface_area DECIMAL NOT NULL,
    capital VARCHAR(50),
    independence_year INT NOT NULL,
    PRIMARY KEY(isocode)
);
CREATE TABLE country_language (
    country_language VARCHAR(50) NOT NULL,
    --language is a reserved keyword
    spoken_percentage DECIMAL(3, 2) NOT NULL,
    PRIMARY KEY(country_language)
);
CREATE TABLE speaks (
    country_language VARCHAR(50) NOT NULL,
    --language is a reserved keyword
    country_isocode CHAR(3) NOT NULL,
    isofficial BOOLEAN NOT NULL,
    spoken_percentage DECIMAL(3, 2) NOT NULL,
    PRIMARY KEY()
);
--INSERTS
INSERT IGNORE INTO country
VALUES(
        'DNK',
        '0045',
        'Denmark',
        'Danmark',
        5792202,
        81.5,
        'Europe',
        'Europe',
        'Mette Frederiksen',
        'Democracy',
        325000000000,
        355200000000,
        42430,
        'Copenhagen',
        1219
    );
-- INSERT INTO country_language
-- VALUES();